#!/usr/bin/env -S bash -p
#------------------------------------------------------------------------------
# Description: A zRAM installation script
# Author: iDes3rt <ides3rt@protonmail.com>
# Source: https://codeberg.org/ides3rt/Extras
# Last Modified: 2022-08-03
#------------------------------------------------------------------------------
# Dependencies:
#       bash
#       coreutlis
#------------------------------------------------------------------------------

declare -r prog=${0##*/}

die ()
{
	echo "$prog: $2" >&2
	(( $1 )) &&
		exit $1
}

((EUID)) &&
	die 1 'required root privileges.'

(( $# )) &&
	die 1 "needn't argument."

for d in {modules-load,modprobe,sysctl,udev/rules}.d; do
	[[ -d /etc/$d ]] ||
		mkdir -p /etc/"$d"
done

read -d '' <<-EOF
#------------------------------------------------------------------------------
# Description: Configuration for zRAM-related modules
# Author: iDes3rt <ides3rt@protonmail.com>
# Source: https://codeberg.org/ides3rt/Extras
# Last Modified: 2022-06-17
#------------------------------------------------------------------------------

zram
EOF
echo -n "$REPLY" > /etc/modules-load.d/zram.conf

read -d '' <<-EOF
#------------------------------------------------------------------------------
# Description: Configuration for zRAM-related Modprobe
# Author: iDes3rt <ides3rt@protonmail.com>
# Source: https://codeberg.org/ides3rt/Extras
# Last Modified: 2022-06-17
#------------------------------------------------------------------------------

options zram num_devices=1
EOF
echo -n "$REPLY" > /etc/modprobe.d/99-zram.conf

read _ iec _ < /proc/meminfo
iec=$(numfmt --from-unit=K --to=iec "$iec")

unit=${iec##*[0-9]}
doub_iec=$(( ${iec%?} * 2 ))

read -d '' <<-EOF
#------------------------------------------------------------------------------
# Description: Configuration for zRAM-related Udev-rules
# Author: iDes3rt <ides3rt@protonmail.com>
# Source: https://codeberg.org/ides3rt/Extras
# Last Modified: 2022-06-17
#------------------------------------------------------------------------------

KERNEL=="zram0", ATTR{comp_algorithm}="zstd", ATTR{disksize}="$doub_iec$unit", RUN="/sbin/mkswap /dev/zram0", TAG+="systemd"
EOF
echo -n "$REPLY" > /etc/udev/rules.d/99-zram.rules

unset -v iec unit doub_iec
echo '/dev/zram0 none swap pri=32767 0 0' >> /etc/fstab

read -d '' <<-EOF
#------------------------------------------------------------------------------
# Description: Configuration for zRAM-related Sysctl
# Author: iDes3rt <ides3rt@protonmail.com>
# Source: https://codeberg.org/ides3rt/Extras
# Last Modified: 2022-06-17
#------------------------------------------------------------------------------

vm.swappiness = 200
vm.vfs_cache_pressure = 200
vm.page-cluster = 0
EOF
echo -n "$REPLY" > /etc/sysctl.d/99-zram.conf
