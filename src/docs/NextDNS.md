# The Recommended Configuration for NextDNS by iDes3rt

### Security

* Threat Intelligence Feeds
* Google Safe Browsing
* Cryptojacking Protection
* DNS Rebinding Protection
* IDN Homograph Attacks Protection
* Typosquatting Protection
* Domain Generation Algorithms (DGAs) Protection
* Block Parked Domains
* Block Child Sexual Abuse Material

### Privacy

* Blocklists:
	* NextDNS Ads & Trackers Blocklist
	* AdGuard DNS filter
	* Steven Black
	* oisd
	* Goodbye Ads
	* notracking
	* 1Hosts (Pro)

* Block Disguised Third-Party Trackers

### Settings

* Logs:
	* Disable Logs

* Performence:
	* Anonymized EDNS Client Subnet
	* Cache Boost
	* CNAME Flattening
